CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT
    c.name AS category,
    SUM(p.amount) AS total_sales_revenue
FROM
    payment p
JOIN
    rental r ON p.rental_id = r.rental_id
JOIN
    inventory i ON r.inventory_id = i.inventory_id
JOIN
    film f ON i.film_id = f.film_id
JOIN
    film_category fc ON f.film_id = fc.film_id
JOIN
    category c ON fc.category_id = c.category_id
WHERE
    EXTRACT(QUARTER FROM p.payment_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
GROUP BY
    c.name
HAVING
    SUM(p.amount) > 0; -- Only categories with at least one sale
	
	
	CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_qtr INT)
RETURNS TABLE (category TEXT, total_sales_revenue NUMERIC)
AS $$
BEGIN
    RETURN QUERY 
    SELECT
        c.name AS category,
        SUM(p.amount) AS total_sales_revenue
    FROM
        payment p
    JOIN
        rental r ON p.rental_id = r.rental_id
    JOIN
        inventory i ON r.inventory_id = i.inventory_id
    JOIN
        film f ON i.film_id = f.film_id
    JOIN
        film_category fc ON f.film_id = fc.film_id
    JOIN
        category c ON fc.category_id = c.category_id
    WHERE
        EXTRACT(QUARTER FROM p.payment_date) = current_qtr
        AND EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
    GROUP BY
        c.name
    HAVING
        SUM(p.amount) > 0;
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION new_movie(movie_title TEXT)
RETURNS VOID AS $$
DECLARE
    language_id INT;
    new_film_id INT;
BEGIN
    -- Check if the language exists in the language table
    SELECT language_id INTO language_id FROM language WHERE name = 'Klingon';
    
    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language does not exist in the language table';
    END IF;

    -- Generate a new unique film ID using a sequence
    SELECT nextval('film_film_id_seq') INTO new_film_id;

    -- Insert the new movie into the film table
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), language_id);
END;
$$ LANGUAGE PLPGSQL;